﻿using System;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Drawing;
using System.Runtime.InteropServices;

namespace HierarchyFormsApp
{
    public partial class Form1 : Form
    {
        const string FILENAME = @"MainFile_New.xml";
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();

                var fs = File.ReadAllText(FILENAME, Encoding.UTF8);
                xmldoc.LoadXml(fs);

                treeView2.Nodes.Clear();

                treeView2.Nodes.Add(new TreeNode(xmldoc.DocumentElement.Name));

                TreeNode tNode;

                tNode = treeView2.Nodes[0];

                AddNode(xmldoc.DocumentElement, tNode);
            }
            catch (XmlException xmlEx)
            {
                MessageBox.Show(xmlEx.Message);
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Application.Exit();
            }
        }

        private void AddNode(XmlNode inXmlNode, TreeNode inTreeNode)
        {
            XmlNode xNode;
            TreeNode tNode;
            XmlNodeList nodeList;

            if (inXmlNode.HasChildNodes)
            {
                nodeList = inXmlNode.ChildNodes;
                for (int i = 0; i <= nodeList.Count - 1; i++)
                {
                    xNode = inXmlNode.ChildNodes[i];
                    inTreeNode.Nodes.Add(new TreeNode(xNode.Name));

                    if (inXmlNode.Attributes.Count != 0 && inTreeNode.Text.Equals("Row"))
                    {
                        inTreeNode.Text =  "(" + inXmlNode.ChildNodes.Count + ")  " + inTreeNode.Text;
                        foreach (XmlAttribute att in inXmlNode.Attributes)
                        {
                            inTreeNode.Text = inTreeNode.Text + " " + att.Name + ": \"" + att.Value +"\"";
                        }
                    }

                    tNode = inTreeNode.Nodes[i];
                    AddNode(xNode, tNode);
                }
            }
            else
            {
                inTreeNode.Text = (inXmlNode.OuterXml).Trim();
            }
        }
    }
}
